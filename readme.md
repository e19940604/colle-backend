Colle - Management Interface
======

The management interface  of android application [Colle](https://bitbucket.org/e19940604/colle-append/src/master/) is based on laravel and MongoDB. Staff can use this page to manage point cards

Web Interface
===
![](https://i.imgur.com/3w1EMXf.jpg)
![](https://i.imgur.com/5k3ca65.jpg)
![](https://i.imgur.com/mN07CUL.jpg)

Database
===
![](https://i.imgur.com/TbfOK1c.jpg)