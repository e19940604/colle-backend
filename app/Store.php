<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Store extends Eloquent
{
    //
    protected $collection = 'store';

    public function card(){
        return $this->hasMany('App\Card' , 'store_id' , '_id');
    }

    public function gift(){
        return $this->hasMany('App\Gift' , 'store_id' , '_id');
    }
}
