<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Gift extends Eloquent
{
    //
    protected $collection = "gift";

    public function store(){
        return $this->belongsTo('App\Store' );
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
