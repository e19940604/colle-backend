<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class cardController extends Controller
{
	/**
     * add card point
     * @param 
     *  user_id => string // cell phone
     *  store_id => string // must be more than 8
     * @return [
     *      'status' => boolean ( 0 / 1 )
     *      'err_msg' = string
     *  ]
     */
    function addPoint(){
    	$result = [];
    	$result['status'] = false;


    	if( \Input::has('user_id') && \Input::has('store_id')  /* &&  \App\Store::find( \Input::get('store_id') ) !== null  */){
    		$user_id = \Input::get('user_id');
    		$store_id = \Input::get('store_id');

    		$card =  \App\Card::where( 'user_id' , $user_id )->where( 'store_id' , $store_id )->first();
    		if( $card === null ){
    			$card = new \App\Card;
    			$card->user_id = $user_id;
    			$card->store_id = $store_id;
    			$card->point = 1;
    			$card->status_code = 1;
    			$card->save();
    		}
    		else{
                $card->point = $card->point + 1;
                $card->save();
    		}
    		$result['status'] = true;
    		$result['data'] = $card->toArray();
    	}
    	else {
    		$result['err_msg'] = "參數缺少或錯誤";
    	}

    	return \Response::json( $result );
    }

    protected function fastAdd( \Request $request ){

        $points = \Input::get('points');
        $card_id = \Input::get('card_id');

        $card = \App\Card::find( $card_id );
        $card->point = $points;
        $card->save();
    }

    protected function lottery(){

		$list = \App\Card::where( "store_id" , "2F18E35A"  )
			->orWhere( "store_id" , "107D8FDC" )
			->with('user')
			->get();
		$data = [];
		$data['username'] = [];
		foreach( $list as $key => $value ){
			if( $value->user->realname )
				array_push($data['username'] , $value->user->realname  );
			else
				array_push($data['username'] , $value->user->username  );
		}
		$data['totalcount'] = count( $data['username'] );
		$data['hash'] = md5( $data['totalcount'] );

		return  view('lottery')->with('list' , $data );
    }
}
