<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Store;
use App\Gift;
use App\StoreManage;

class DashboardController extends Controller
{
    protected function index(){
    	
    	if( Auth::admin()->check() ){
    		$admin = Auth::admin()->user();
    		$store_id = $admin->store_id;
    		$store = Store::find( $store_id );
    		$result = [
    			'name' => $admin->name,
    			'store_name' => $store->store_name,
    		];
    		return view('index' , $result );
    	}

    	return view('index');
    }

        protected function manageLogin( Request $request){
        
        $rule = [
            'username' => 'required',
            'password' => 'required'
        ];

        $messages = [
            'required' => ':attribute 不可為空白。'
        ];

        $validator = \Validator::make( $request->all() , $rule , $messages );

        if( $validator->fails() ){
            $errMsg = "";

            foreach ( $validator->errors()->getMessages() as  $value) {
                $errMsg .= $value[0] . '\n';
            }
            return back()->with( 'err' , $errMsg );
        }

        $userData = [
            'username' => $request->get('username'),
            'password' => $request->get('password')
        ];

        if( \Auth::admin()->attempt( $userData ) ){
            return redirect()->intended('/');
        }
        else{
            return back()->with( 'err' , "帳號或密碼錯誤。" );    
        }
    }

	protected function logout(){
		Auth::admin()->logout();
		return redirect()->intended('/');
	}

    protected function getGift(){
        $result = [];
        $result['status'] = 'success';
        try{
            $currentUser = Auth::admin()->user();
            $gifts = Gift::where( 'store_id' , $currentUser->store_id )->with('User')->get();
            $result['data'] = [];

            foreach( $gifts as $row ){
                $array = [];
                $row_user = $row->user;
                if( $row_user == null ){
                    $array['phone'] = "（空）";
                    $array['name'] = "（空）";
                    $array['created_at'] = $row->created_at;
                }
                else{
                    $array['phone'] = $row_user->username;
                    if( $row_user->realname ){
                        $array['name'] = $row_user->realname;
                        $array['name'] = substr_replace( $array['name'] , "Ｏ" , 3 , 3 );
                    }
                    else{
                        $array['name'] = "（空）";
                    }
                    $array['created_at'] = Carbon::parse( $row_user->created_at )->format("Y-m-d");
                }
                array_push( $result['data'] , $array );
            }
        }
        catch( \Exception $e ){
            $result['status'] = 'failure';
            $result['err_msg'] = $e->getMessage();
        }


        return $result;
    }

    protected function findGiftByCode( Request $request ){
        $result = [];
        $result['status'] = 'success';
        $user = Auth::admin()->user();

        try{
            $gift = Gift::where('serialNum' , $request->get('serialNum') )->where('store_id' , $user->store_id )->with('User')->first();

            if( $gift != null ){
                $result['data'] = [
                    'name' => $gift->user->realname,
                    'phone' => $gift->user->username,
                    'created_at' => Carbon::parse( $gift->created_at )->format("Y-m-d")
                ];
            }
            else{
                $result['data'] = $gift;
            }
        }
        catch( \Exception $e ){
            $result['status'] = 'failure';
            $result['err_msg'] = $e->getMessage();
        }

        return response()->json( $result );
    }

    protected function decideToExchange( Request $request ){
        $result = [];
        $result['status'] = 'success';

        try{
            Gift::where('serialNum' , $request->get('serialNum') )->delete();
        }
        catch( \Exception $e ){
            $result['status'] = 'failure';
            $result['err_msg'] = $e->getMessage();
        }

        return response()->json( $result );
    }
}
