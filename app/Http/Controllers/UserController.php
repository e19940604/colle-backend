<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class UserController extends Controller
{

    /**
     * creating a new user
     * @param 
     *  username => string // cell phone
     *  password => string // must be more than 8
     *  realname => string
     *  email => string
     *  nickname => string
     * @return [
     *      'status' => boolean ( 0 / 1 )
     *      'err_msg' = string
     *  ]
     */
    public function create()
    {
        $result = [];
        $result['status'] = false;

        $user = new \App\User;
        $user->username = \Input::get('username');
        $user->password = \Hash::make( \Input::get('password') );
        $user->email = \Input::get('email');
        \Input::get('nickname') ? $user->nickname = \Input::get('nickname') : $user->nickname = null;
        \Input::get('realname') ? $user->realname = \Input::get('realname') : $user->realname = null;
        $user->is_veri = false;
        $user->veri_code = substr( md5( time().$user['username'] ) , 0 , 4 );

        try{
            $user->save();
        }
        catch( \Exception $e ){
            $result['err_msg'] = $e->getMessage();
            return \Response::json( $result );
        }

        $result['status'] = true;

        return \Response::json( $result );
    }

    /**
     * login
     * @param 
     *  username => string // cell phone
     *  password => string // must be more than 8
     * @return [
     *      'status' => boolean ( 0 / 1 )
     *      'err_msg' = string
     *  ]
     */
    public function login(){

        $result = [];
        $result['status'] = false;

        if( !\Input::has('username') || !\Input::has('password') ){
            $result['err_msg'] = "尚有欄位空白";
        }
        else{
            $user = [
                'username' => \Input::get('username'),
                'password' => \Input::get('password')
            ];

            if( !\Auth::user()->attempt( $user ) )
                $result['err_msg'] = "帳號或密碼錯誤";
            else{
                $result['status'] = true;
                $logged_user = \App\User::where( 'username' , $user['username'] )->first();
                $result['data'] = [];
                $result['data']['user'] = [];
                $result['data']['user']['user_id'] = $logged_user->id;
                $result['data']['user']['username'] = $logged_user->username;
                $result['data']['user']['email'] = $logged_user->email;
                $result['data']['user']['realname'] = $logged_user->realname;
                $result['data']['user']['nickname'] = $logged_user->nickname;

                $cards = \App\Card::where( 'user_id' , $logged_user->id )->get();
                $result['data']['cards'] = [];
                foreach ($cards as $key => $value ) {
                    
                    $card = [];
                    $card['card_id'] = $value->id;
                    $card['user_id'] = $value->user_id;
                    $card['store_id'] = $value->store_id;
                    $card['store_id'] = $value->store_id;
                    $card['point'] = $value->point;
                    $card['status_code'] = $value->status_code;

                    array_push( $result['data']['cards'] , $card  );
                }
            }
        }
        return \Response::json( $result );
    }

    /**
     * isLogin
     * @return [
     *      'status' => boolean ( 0 / 1 )
     *      'err_msg' = string
     *      'isLogin' => boolean ( 0 / 1 )
     *  ]
     */
    public function isLogin(){
        $result = [];
        $result['status'] = false;
        try{
            if( \Auth::user()->check() ) 
                $result['isLogin'] = true;
            else
                $result['isLogin'] = false;

            $result['status'] = true;
        }
        catch( \Exception $e ){
            $result['err_msg'] = $e->getMessage();
        }
        
        return \Response::json( $result );
    }

    /**
     * logout
     * @return [
     *      'status' => boolean ( 0 / 1 )
     *      'err_msg' = string
     *  ]
     */
    protected function logout(){
        $result = [];
        try {
            \Auth::user()->logout();
        } catch (Exception $e) {
            $result['status'] = false;
        }
        $result['status'] = true;
        return \Response::json( $result );
    }

}
