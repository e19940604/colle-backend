<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Mockery\CountValidator\Exception;
use App\Card;
use App\User;
use App\Store;
use Carbon\Carbon;

class StoreController extends Controller
{
    protected function getStore(){
    	$result = [];
    	$result['status'] = "fail";
    	try {
    		$stores = \App\Store::all();
    	} catch (Exception $e) {
    		$result['error'] = $e->getMessages();
    		return response()->json( $result );
    	}
    	
    	if( $stores != null ){
    		$stores = $stores->toArray();
    	}
    	else{
    		$result['error'] = "No any store data";
    		return response()->json($result);
    	}
    	
    	$result['data'] = $stores;
    	$result['status'] = "success";
    	return response()->json( $result );
    }

	protected function exchangeGift( Request $request ){
        $result= [];
        $result['status'] = false;
        $user = \Auth::user()->getUser();

        $rule = [
            'user_id' => 'required',
            'card_id' => 'required',
        ];

        $validator = Validator::make( $request->all() , $rule );

        if( $validator->fails() ){
            $result['err_msg'] = $validator->errors();
            return response()->json( $result );
        }

        try{
            $card = Card::find( $request->get('card_id') );
        }
        catch( ModelNotFoundException $e ){
            $result['err_msg'] = 'not found card_id ';
            return response()->json( $result );
        }

        try{
            $user = User::find( $request->get('user_id') );
        }
        catch( ModelNotFoundException $e ){
            $result['err_msg'] = 'not found user_id ';
            return response()->json( $result );
        }

        if( $card->point < 10 ){
            $result['err_msg'] = "not enough point";
            return response()->json( $result );
        }

        if( $user->id != $request->get('user_id') ){
            $result['err_msg'] = 'user_id is uncorrect';
            return response()->json( $result) ;
        }

        $card->point = $card->point - 10;


        $store = $card->store()->get()->toArray();
        $gift = new \App\Gift;

        $gift->store_id = $store[0]['_id'];
        $gift->user_id = $user->_id;

        $gift->serialNum = substr( md5( $user->_id . Carbon::now()->format('Y-m-D H:M:s') ) , 0 , 4 );


        $card->save();
        $gift->save();
        $result['status'] = true;
        $result['serialNum'] = $gift->serialNum;

        return response()->json( $result );

    }
}
