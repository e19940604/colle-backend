<?php

namespace App\Http\Middleware;

use Closure;

class UserRegMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $result = [];
        $result['status'] = false;
        $user = \App\User::where('username' , $request->input('username') )->count();

        if( $user > 0 ){
            $result['err_msg'] = "該使用者已註冊";
        }
        else if( !preg_match( "/09[0-9]{8}/" , $request->input('username') ) ){
            $result['err_msg'] = "手機號碼格式錯誤";
        }
        else if( !filter_var( $request->input('email') , FILTER_VALIDATE_EMAIL)  ){
            $result['err_msg'] = "email 格式錯誤";   
        }
        else if( strlen( $request->input('password') ) < 4  ){
            $result['err_msg'] = "密碼長度須為 4 碼以上";
        }
        else{
            return $next($request);
        }
        return \Response::json( $result );
    }
}
