<?php

namespace App\Http\Middleware;

use Closure;

class checkInputExistMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next , $indexArr )
    {
        $result = [];
        $result['status'] = false;
        dd( $indexArr );
        foreach ($indexArr as $index ) {
            if( !$request->has( $index ) ){
                $result['err_msg'] = "參數錯誤或缺少";
                return \Response::json( $result );
            }
        }
        return $next($request);
    }
}
