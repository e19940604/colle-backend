<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * web management
 */

Route::get('/', 'DashboardController@index');
Route::post('/login' , 'DashboardController@manageLogin');
Route::get('/logout' , 'DashboardController@logout');
Route::post('/getGift' , 'DashboardController@getGift' );
Route::post('/findGiftByCode' , 'DashboardController@findGiftByCode');
Route::post('/decideToExchange' , 'DashboardController@decideToExchange');
/**
 * api area
 */

Route::post('/api/user/register' , [ 'middleware' => 'user.register' , 'uses' => 'UserController@create'] );
Route::post('/api/user/isLogin' , [ 'uses' => 'UserController@isLogin']  );
Route::post('/api/user/login' , [ 'uses' => 'UserController@login'] );
Route::post('/api/user/logout' , [ 'uses' => 'UserController@logout'] );
Route::post('/api/exchangeGift' , 'StoreController@exchangeGift' );
Route::post('/api/getStore' , 'StoreController@getStore' );
Route::post('/api/fastAdd' , 'CardController@fastAdd');
Route::post('/api/addPoint' , [ 'uses' => 'CardController@addPoint'] );
Route::get('/api/download' , function(){
	return response()->download('LoyaltyCard.apk');
});

Route::get('/lottery' , 'CardController@lottery' );

Route::post('deploy' , function(){

	try{
		$deployer = new \Tmd\AutoGitPull\Deployer( array(
			'directory' => '/usr/share/nginx/colle-backend/',
			'logDirectory' => '/usr/share/nginx/colle-backend/storage/logs/'
		));

		$deployer->deploy();
		\Log::info('git pull success');
	}
	catch( \Exception $e ){
		\Log::info( $e->getMessage() );
	}

});
