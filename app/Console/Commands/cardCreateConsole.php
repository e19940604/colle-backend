<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class cardCreateConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'card-create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create fake card data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $card = factory( \App\Card::class  )->create();
        echo $card['store_id'];
    }
}
