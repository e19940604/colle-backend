<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
    	$user = factory( \App\User::class )->make();
    	$count = \App\User::find( $user->id );
    	while( $count ){
    		$user = factory( \App\User::class )->make();
    		$count = \App\User::find( $user->id );
    	}

        $this->post('/api/user/register' , $user->toArray() )
             ->seeJson([
             	'status' => true,
             ]);
    }

    public function testReReg(){
    	$user = [
	        'username' => "0912303622" ,
	        'email' => "e19940604@gmail.com",
	        'password' => "123456",
	        'nickname' => "xgnid"
    	];
    	$count = \App\User::where( 'username' , $user['username'] )->count();
 
    	if( $count == 0 ){
    		$u = new \App\User;
    		$u->username = $user['username'];
    		$u->email = $user['email'];
    		$u->password = \Hash::make( $user['password'] );
    		$u->nickname = $user['nickname'];
    		$u->save();
    	}
    	$this->post('/api/user/register' , $user )
             ->seeJson([
             	'status' => false,
             	'err_msg' => "該使用者已註冊"
             ]);
    }

    public function testFailEmail(){
		$user = factory( \App\User::class )->make()->toArray();
		$user['email'] = "DF";
    	$this->post('/api/user/register' , $user )
             ->seeJson([
             	'status' => false,
             	'err_msg' => "email 格式錯誤"
             ]);
    }

    public function testLogin(){

    	$user = [
    		'username' => '0912303622',
    		'password' => '123456'
    	];

    	$this->post('/api/user/login' , $user )
    		 -> seeJson([
    		 	'status' => true,
    		 ]);
    }

    public function testLoginFailed(){
    	$user = [
    		'username' => '0912303622',
    		'password' => '777777'
    	];

    	$this->post('/api/user/login' , $user )
    		 -> seeJson([
    		 	'status' => false,
    		 	'err_msg' => '帳號或密碼錯誤'
    		 ]);
    	
    }
}