<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'username' => "09" . $faker->numberBetween( 10000000 , 99999999 ),
        'email' => $faker->email,        
        'password' => str_random(10) ,
        'nickname' => $faker->name
    ];
});

$factory->define(App\Card::class, function ($faker) {
    return [
        'user_id' => "55a5e18cd7b8e2c85b00002a",
        'store_id' => strval( $faker->randomNumber( 5 ) ),
        'point' => $faker->randomDigitNotNull(),
        'status_code' => 1
    ];
});
