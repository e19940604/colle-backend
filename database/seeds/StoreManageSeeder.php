<?php

use Illuminate\Database\Seeder;
use App\StoreManage;

class StoreManageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manage = new App\StoreManage();
        $manage->username = "test";
        $manage->password = \Hash::make("test");
        $manage->name = "陳重佑";
        $manage->store_id = "8BA70CEC";
        $manage->save();

        $manage = new App\StoreManage();
        $manage->username = "xgnid";
        $manage->password = \Hash::make("xgnid");
        $manage->name = "陳定延";
        $manage->store_id = "5A5AEF7F";
        $manage->save();

        $manage = new App\StoreManage();
        $manage->username = "milktea";
        $manage->password = \Hash::make("milktea");
        $manage->name = "双妃";
        $manage->store_id = "2A035AB0";
        $manage->save();

    }
}
