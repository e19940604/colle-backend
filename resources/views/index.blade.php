<!DOCTYPE HTML>
<!--
	Astral by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>集點趣</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/materialize.css">
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css">
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Wrapper-->
			<div id="wrapper">

				<!-- Nav -->
				@if( Auth::admin()->check() )
					<nav id="nav">
						<a id="me_icon" href="#me" class="icon fa-home active"><span>首頁</span></a>
						<a id="gift_icon" href="#gift" class="icon fa-gift"><span>兌換頁面</span></a>
						<!--<a href="#edit" class="icon fa-pencil-square-o"><span>資料修改</span></a>
						<a href="#" class="icon fa-twitter"><span>Twitter</span></a>-->
					</nav>
				@endif

				<!-- Main -->
					<div id="main">

						<!-- Me -->
							<article id="me" class="panel">
								<header>
									<h2>集點趣</h2>
									<p>Store management page</p>
									@if( Auth::admin()->check() )
										<div class="index-profile">
											<blockquote>
												<h5>店家名稱：{{ $store_name }}</h5>
												<h5>員工姓名：{{ $name }}</h5>
											</blockquote>
												
										</div>
									@else
									<div class="form-container">
										
										<form class="col s12" method="post" action="login">
											<input type="hidden" name="_token" value="{{ csrf_token() }}" >
											<div class="row">
												<div class="input-field col l8 s11">
													<input id="username" name="username" type="text" class="validate">
													<label for="username">帳號：</label>
												</div>
											</div>
											<div class="row">
												<div class="input-field col l8 s11">
													<input id="password"  name="password" type="password" class="validate">
													<label for="password">密碼：</label>
												</div>
											</div>
											<div class="row">
												<div class="input-field col l3  m3 s4 offset-s8 offset-m9 offset-l6">
													<button class="btn waves-effect waves-light" type="submit" name="action">登入
														<i class="material-icons right">send</i>
													</button>
												</div>
											</div>
										</form>
									</div>
									@endif
								</header>
								<a href="#" class="z-depth-2 pic">
									<!--<span class="arrow icon fa-chevron-right"><span>See my work</span></span>-->
									<img  class="waves-effect waves-light" src="images/Final.png" alt="" />
								</a>
							</article>
						@if( Auth::admin()->check() )
						<!-- Work -->
							<article id="gift" class="panel">
								<div class="row">
									<div class="col s12 m12 l7 ">
										<header>
											<h2>兌換頁面</h2>
										</header>
										<p>輸入驗證碼<br><!--<small><del>換一個不夠你有換兩個嘛？</del></small>--></p>
										<form id="gift-form">
											<input type="hidden" name="_token" value="{{ csrf_token() }}" >
											<div class="input-field col s11">
												<input id="serialNum" name="serialNum" type="text" class="validate">
												<label for="serialNum">序號 ：</label>
											</div>
										</form>
                                        <div class="input-field col s11 ">
                                            <button id="veri-btn" class="btn waves-effect waves-light"  name="action">送出
                                                <i class="material-icons right">send</i>
                                            </button>
                                        </div>
									</div>
									<div id="gift-table" class="col s12 m12 l5">
										<header>
											<h4>目前可兌換名單</h4>
										</header>
                                        <div class="tbContent">
                                            <div id="waiting-icon" >
                                                <i class="fa fa-spinner fa-pulse"></i>
                                            </div>
                                        </div>

										
										<!--<table class="bordered highlight"> 
											<thead>
												<tr>
													<th class="column">電話</th>
													<th class="column">姓名</th>
													<th class="column">兌換時間</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td data-title="Name">0912303622</td>
													<td data-title="Email">陳Ｏ延</td>
													<td data-title="Phone"> 2014-05-06</td>
												</tr>
												<tr>
													<td data-title="Name">0912303622</td>
													<td data-title="Email">陳Ｏ延</td>
													<td data-title="Phone"> 2014-05-06</td>
												</tr>
												<tr>
													<td data-title="Name">0912303622</td>
													<td data-title="Email">陳Ｏ延</td>
													<td data-title="Phone"> 2014-05-06</td>
												</tr>
												<tr>
													<td data-title="Name">0912303622</td>
													<td data-title="Email">陳Ｏ延</td>
													<td data-title="Phone"> 2014-05-06</td>
												</tr>
											</tbody>
										</table>-->
									</div>
								</div>
							</article>

						<!-- Contact -->
							<article id="edit" class="panel">
								<header>
									<h2>Contact Me</h2>
								</header>
								<form action="#" method="post">
									<div>
										<div class="row">
											<div class="6u 12u$(mobile)">
												<input type="text" name="name" placeholder="Name" />
											</div>
											<div class="6u$ 12u$(mobile)">
												<input type="text" name="email" placeholder="Email" />
											</div>
											<div class="12u$">
												<input type="text" name="subject" placeholder="Subject" />
											</div>
											<div class="12u$">
												<textarea name="message" placeholder="Message" rows="8"></textarea>
											</div>
											<div class="12u$">
												<input type="submit" value="Send Message" />
											</div>
										</div>
									</div>
								</form>
							</article>
						@endif
					</div>

				<!-- Footer -->
					<div id="footer">
						<ul class="copyright">
							<li>Copyright &copy; 2015 by Xgnid & ChungYu </li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
						</ul>
					</div>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/skel-viewport.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
			<script type="text/javascript" src="assets/js/materialize.js"></script>
			<script src="dist/sweetalert.min.js"></script> 
			@if( session('err') )

				<script type="text/javascript">
					var error_msg = "{{  session('err')   }}";
					sweetAlert( "Oops...." , error_msg , "error");
				</script>
			@endif
			@if( Auth::admin()->check() )
				<script type="text/javascript">
/*
 .-----------------..----------------. .----------------. .----------------. .----------------. .----------------. .----------------. .----------------.
 | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. |
 | | ____  _____  | | |    _______   | | |  ____  ____  | | |    _______   | | | _____  _____ | | |     ______   | | |    _______   | | |  _________   | |
 | ||_   \|_   _| | | |   /  ___  |  | | | |_  _||_  _| | | |   /  ___  |  | | ||_   _||_   _|| | |   .' ___  |  | | |   /  ___  |  | | | |_   ___  |  | |
 | |  |   \ | |   | | |  |  (__ \_|  | | |   \ \  / /   | | |  |  (__ \_|  | | |  | |    | |  | | |  / .'   \_|  | | |  |  (__ \_|  | | |   | |_  \_|  | |
 | |  | |\ \| |   | | |   '.___`-.   | | |    \ \/ /    | | |   '.___`-.   | | |  | '    ' |  | | |  | |         | | |   '.___`-.   | | |   |  _|  _   | |
 | | _| |_\   |_  | | |  |`\____) |  | | |    _|  |_    | | |  |`\____) |  | | |   \ `--' /   | | |  \ `.___.'\  | | |  |`\____) |  | | |  _| |___/ |  | |
 | ||_____|\____| | | |  |_______.'  | | |   |______|   | | |  |_______.'  | | |    `.__.'    | | |   `._____.'  | | |  |_______.'  | | | |_________|  | |
 | |              | | |              | | |              | | |              | | |              | | |              | | |              | | |              | |
 | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' |
 '----------------' '----------------' '----------------' '----------------' '----------------' '----------------' '----------------' '----------------'
 .----------------. .----------------. .----------------. .----------------. .----------------. .----------------. .----------------. .----------------.
 | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. |
 | |     ______   | | |     ____     | | |   _____      | | |   _____      | | |  _________   | | |      __      | | |   ______     | | |   ______     | |
 | |   .' ___  |  | | |   .'    `.   | | |  |_   _|     | | |  |_   _|     | | | |_   ___  |  | | |     /  \     | | |  |_   __ \   | | |  |_   __ \   | |
 | |  / .'   \_|  | | |  /  .--.  \  | | |    | |       | | |    | |       | | |   | |_  \_|  | | |    / /\ \    | | |    | |__) |  | | |    | |__) |  | |
 | |  | |         | | |  | |    | |  | | |    | |   _   | | |    | |   _   | | |   |  _|  _   | | |   / ____ \   | | |    |  ___/   | | |    |  ___/   | |
 | |  \ `.___.'\  | | |  \  `--'  /  | | |   _| |__/ |  | | |   _| |__/ |  | | |  _| |___/ |  | | | _/ /    \ \_ | | |   _| |_      | | |   _| |_      | |
 | |   `._____.'  | | |   `.____.'   | | |  |________|  | | |  |________|  | | | |_________|  | | ||____|  |____|| | |  |_____|     | | |  |_____|     | |
 | |              | | |              | | |              | | |              | | |              | | |              | | |              | | |              | |
 | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' |
 '----------------' '----------------' '----------------' '----------------' '----------------' '----------------' '----------------' '----------------'
                                                .----------------. .----------------. .----------------.
                                                | .--------------. | .--------------. | .--------------. |
                                                | |   _    _     | | |     ____     | | |   _    _     | |
                                                | |  | |  | |    | | |   .'    '.   | | |  | |  | |    | |
                                                | |  | |__| |_   | | |  |  .--.  |  | | |  | |__| |_   | |
                                                | |  |____   _|  | | |  | |    | |  | | |  |____   _|  | |
                                                | |      _| |_   | | |  |  `--'  |  | | |      _| |_   | |
                                                | |     |_____|  | | |   '.____.'   | | |     |_____|  | |
                                                | |              | | |              | | |              | |
                                                | '--------------' | '--------------' | '--------------' |
                                                '----------------' '----------------' '----------------'
 .-----------------..----------------. .----------------. .----------------. .----------------. .----------------. .-----------------..----------------.
 | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. |
 | | ____  _____  | | |     ____     | | |  _________   | | |  _________   | | |     ____     | | | _____  _____ | | | ____  _____  | | |  ________    | |
 | ||_   \|_   _| | | |   .'    `.   | | | |  _   _  |  | | | |_   ___  |  | | |   .'    `.   | | ||_   _||_   _|| | ||_   \|_   _| | | | |_   ___ `.  | |
 | |  |   \ | |   | | |  /  .--.  \  | | | |_/ | | \_|  | | |   | |_  \_|  | | |  /  .--.  \  | | |  | |    | |  | | |  |   \ | |   | | |   | |   `. \ | |
 | |  | |\ \| |   | | |  | |    | |  | | |     | |      | | |   |  _|      | | |  | |    | |  | | |  | '    ' |  | | |  | |\ \| |   | | |   | |    | | | |
 | | _| |_\   |_  | | |  \  `--'  /  | | |    _| |_     | | |  _| |_       | | |  \  `--'  /  | | |   \ `--' /   | | | _| |_\   |_  | | |  _| |___.' / | |
 | ||_____|\____| | | |   `.____.'   | | |   |_____|    | | | |_____|      | | |   `.____.'   | | |    `.__.'    | | ||_____|\____| | | | |________.'  | |
 | |              | | |              | | |              | | |              | | |              | | |              | | |              | | |              | |
 | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' |
 '----------------' '----------------' '----------------' '----------------' '----------------' '----------------' '----------------' '----------------'
 */
					(function(){
						var cache = {};
                        /*cache['giftData'] = '<table class="bordered highlight">' +
                                '<thead>' +
                                '<tr>' +
                                '<th class="column">電話</th>' +
                                '<th class="column">姓名</th>' +
                                '<th class="column">兌換時間</th>' +
                                '</tr>' +
                                '</thead>' +
                                '<tbody>' +
                                '<tr>' +
                                '<td data-title="Name">0912303622</td>' +
                                '<td data-title="Email">陳Ｏ延</td>' +
                                '<td data-title="Phone"> 2014-05-06</td>' +
                                '</tr>' +
                                '</tbody>' +
                                '</table>';*/
						var gift_icon = document.getElementById('gift_icon');
                        var gift_tbContent = $('.tbContent');
                        var veri_btn = $('#veri-btn');

                        veri_btn.on('click' , function(){
                            var data = $('#gift-form').serialize();
                            $.post(
                                '/findGiftByCode' , data , function( result ){
                                    if( result['status'] == "success"){
                                        if( result['data'] == null ){
                                            sweetAlert("Oops..." , "找不到對應的序號" , "error" );
                                        }
                                        else{

                                            swal({   title: "確定要兌換？",
                                                text: "請確認兌換者的資料：\n" +
                                                "電話：" + result['data']['phone'] +
                                                "\n姓名：" + result['data']['name'] +
                                                "\n兌換日期：" + result['data']['created_at'],
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#52CE89",
                                                confirmButtonText: "確認無誤",
                                                cancelButtonOnText: "取消" ,
                                                closeOnConfirm: false
                                            }, function(){
                                                $.post(
                                                    '/decideToExchange' , data , function( result ){
                                                        if( result['status'] == "success" ){
                                                            swal("兌換成功!", "你已經成功兌換。", "success");
                                                            location.reload();
                                                        }
                                                        else{
                                                            sweetAlert("Oops..." , "Something went wrong! please try again later." , "error" );
                                                            console.log( data['err_msg'] );
                                                        }
                                                    }
                                                );

                                            });
                                        }
                                    }
                                    else{
                                        sweetAlert("Oops...", "Something went wrong!", "error");
                                        console.log( data['err_msg'] );
                                    }
                                } , "json"
                            );
                        });

                        if( !('giftData' in cache) ){
                            $.post(
                                '/getGift' , null , function( result ){
                                    if( result['status'] == "success" ){

                                        gift_tbContent.empty();
                                        cache['giftData'] = format_gift_data( result['data'] );
                                        gift_tbContent.append( cache['giftData']);
                                    }
                                    else{
                                        sweetAlert( "Oops...." , result['err_msg'] , "error");
                                    }
                                } , "json"
                            );
                        }
                        else{
                            var gift_tbContent = $('.tbContent');
                            gift_tbContent.empty();
                               gift_tbContent.append( cache['giftData']);
                        }


                        var format_gift_data = function( data ){
                            var string = '<table class="bordered highlight">' +
                                    '<thead>' +
                                    '<tr>' +
                                    '<th class="column">電話</th>' +
                                    '<th class="column">姓名</th>' +
                                    '<th class="column">兌換時間</th>' +
                                    '</tr>' +
                                    '</thead>' +
                                    '<tbody>';
                            for( var key in data ){
                                string += '<tr>' +
                                        "<td data-title=\"Name\">" + data[key]['phone'] + "</td>" +
                                        "<td data-title=\"Email\">" + data[key]['name'] + "</td>" +
                                        "<td data-title=\"Phone\">" + data[key]['created_at'] + "</td>" +
                                        "</tr>";
                            }

                            /*string += '<tr>' +
                            '<td data-title="Name">0912303622</td>' +
                            '<td data-title="Email">陳Ｏ延</td>' +
                            '<td data-title="Phone"> 2014-05-06</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td data-title="Name">0912303622</td>' +
                            '<td data-title="Email">陳Ｏ延</td>' +
                            '<td data-title="Phone"> 2014-05-06</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td data-title="Name">0912303622</td>' +
                            '<td data-title="Email">陳Ｏ延</td>' +
                            '<td data-title="Phone"> 2014-05-06</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td data-title="Name">0912303622</td>' +
                            '<td data-title="Email">陳Ｏ延</td>' +
                            '<td data-title="Phone"> 2014-05-06</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td data-title="Name">0912303622</td>' +
                            '<td data-title="Email">陳Ｏ延</td>' +
                            '<td data-title="Phone"> 2014-05-06</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td data-title="Name">0912303622</td>' +
                            '<td data-title="Email">陳Ｏ延</td>' +
                            '<td data-title="Phone"> 2014-05-06</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td data-title="Name">0912303622</td>' +
                            '<td data-title="Email">陳Ｏ延</td>' +
                            '<td data-title="Phone"> 2014-05-06</td>' +
                            '</tr>';*/

                            string +='</tbody>' +
                                    '</table>';

                            return string;
                        };
					})();
				</script>
			@endif
	</body>
</html>