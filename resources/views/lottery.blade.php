<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" integrity="sha384-aUGj/X2zp5rLCbBxumKTCw2Z50WgIr1vs/PFN4praOTvYXWlVyh2UtNUU0KAUhAX" crossorigin="anonymous">
    <style type="text/css">
        body {
            background-color: #C6E6F3;
        }

        .main{
            width: 50%;
            margin: auto;
        }

        .main > h1 {
            text-align: center;
        }

        #result {
            font-size: 30px;
            color: #F32A35;
        }


    </style>

</head>

<body>

<div class="main">
    <h1>公平隨機抽獎產生器</h1>

    <form>
        <div class="form-group">
            <label for="title">抽獎標題</label>
            <input id="title" class="form-control" type="text" value="集點趣電影票抽獎活動"/>
        </div>
        <div class="form-group">
            <label for="total">參加人數</label>
            <input type="text" class="form-control" id="total" value={{ $list['totalcount'] }} />
        </div>
        <div class="form-group">
            <label for="num">抽幾個人</label>
            <input type="text" class="form-control" id="num" value="1" />
        </div>

        <div class="form-group">
            <input id="str" class="form-control" type="hidden" value={{ $list['hash'] }} /> <span class="help" data-target="input-str">計算方式為 md5(總人數) </span><br>
        </div>


        <input type="button" value="產生" id="gen" /><br>
    </form>
    <div id="result">


    </div>
    <a href="http://ppt.cc/Zao7q" id="permalink" />隨機程式原始碼 http://ppt.cc/Zao7q </a>
    <div class="summary">

        <h2>抽獎名單</h2>
        <ol>
            @foreach( $list['username'] as $name )
                <li> {{ $name }} </li>
            @endforeach
        </ol>
    </div>
</div>



<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="https://raw.github.com/placemarker/jQuery-MD5/master/jquery.md5.js"></script>
<script>
    function hexdec (hex_string) {
        // Returns the decimal equivalent of the hexadecimal number
        //
        // version: 1109.2015
        // discuss at: http://phpjs.org/functions/hexdec
        // +   original by: Philippe Baumann
        // *     example 1: hexdec('that');
        // *     returns 1: 10
        // *     example 2: hexdec('a0');
        // *     returns 2: 160
        hex_string = (hex_string + '').replace(/[^a-f0-9]/gi, '');
        return parseInt(hex_string, 16);
    }
</script>
<script>
    $(".help").click(function(){
        console.log(id);
        var id = $(this).data('target');
        $("#"+id+ " > span").animate({"left": "+=50px"}, "slow").animate({"left": "-=50px"}, "slow");
    });

    var fields = ["title", "str", "total", "num"];

    // parse hash
    var m = location.href.match(/#(.+)/);
    if(m!==null)
    {
        var qs = m[1];
        for(var i=0; i < fields.length; i++)
        {
            var id = fields[i];
            m = qs.match(new RegExp("&?" + id + "=([^&]+)"));
            if(m!==null)
                $("#" + id).val(decodeURIComponent(m[1]));

        }
        setTimeout(function(){
            $("#gen").trigger('click');
        }, 500);
    }

    $("#gen").click(function(){
        var title = $("#title").val(),
                str = $("#str").val(),
                total = $("#total").val(),
                num = $("#num").val();
        if(str=="")
        {
            alert("請輸入一個字串");
            $("#str").focus();
            return;
        }
        else if(total.match(/^\d+$/)==null)
        {
            alert("請輸入數字");
            $("#total").focus();
            return;
        }
        else if(num.match(/^\d+$/)==null)
        {
            alert("請輸入數字");
            $("#num").focus();
            return;
        }
        str += (new Date).getTime().toString();
        num = parseInt(num);
        total = parseInt(total);

        if(num > total)
        {
            alert('人數不合理'+num+"#"+total);
            return;
        }

        var candidates = new Array();
        for(var i=1; i<= total; i++)
        {
            candidates.push(i);
        }
        //console.log(candidates.join(','));

        var result = [];
        for(var i=1; i <= num; i++)
        {
            var hash = $.md5(str + i),
                    hash_num = hexdec(hash.substring(0,10)),
                    rand = hash_num % candidates.length;
            //console.log((str+i) + "#" +hash,"#", hash_num,"#",rand,"#",candidates);
            result.push(
                    candidates.splice( rand, 1)
            );
        }
        $("#result").html("抽中的人是第 " + result.join(", ") + " 位");

    });

    // update permalink
    $("#" + fields.join(",#")).bind("keyup change", function(){
        var qs = [];
        for(var i=0; i< fields.length; i++)
        {
            var id = fields[i];
            var val = $("#"+id).val();
            if(val!='')
                qs.push(id + "=" + encodeURIComponent(val));

        }
        if(qs.length > 0)
        {
            qs = qs.join('&');
            $("#permalink").attr('href', location.href.split("#")[0] + "#" + qs);
            location.hash = qs;
        }
    });

    // init permalink
    $("#permalink").attr('href', location.href);

    $("#tinyurl").click(function(){
        $(this).attr('href', "http://ppt.cc/gen.php?r=1&t=1&s=" + encodeURIComponent(location.href));
    });

</script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
</body>

</html>